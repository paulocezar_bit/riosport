<?php get_header(); ?>

<div class="gridlock atividades">
<div class="rsc_zebra odd barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Fênix</h3>
<p>Já imaginou a fusão de várias atividades em um espaço moderno e bem equipado? Assim é o Fênix. Uma atividade em um espaço super equipado para aperfeiçoar a execução dos seus movimentos e, consequentemente, o seu condicionamento. Além de tudo isso, ainda é ótima para quem busca aulas com bom gasto calórico.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Marcos-Cordeiro.png');"></div>
<div style="width: 260px;" class="professor-nome">Marcos Cordeiro<span class="professor-detail">Responsável pelo Projeto Fênix</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0021_fenix.jpg');"></div>
</div>
<div class="rsc_zebra even barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>GAP</h3>
<p>Para quem busca uma atividade aeróbica, com gasto calórico e focada em exercícios que trabalhem os glúteos, abdômen e pernas, essa modalidade de ginástica localizada é eficiente. E o melhor: com a criatividade dos professores da RSC, cada aula é diferente da anterior.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Thiago-Freitas.png');"></div>
<div style="width: 260px;" class="professor-nome">Thiago Freitas<span class="professor-detail">Responsável pelo setor de Fitness, planejamento de aulas coletivas, periodizações e musculação.</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0016_GAP.jpg');"></div>
</div>
<div class="rsc_zebra odd barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>GA</h3>
<p>Essa modalidade de ginástica localizada trabalha os glúteos e abdômen e promete aumentar a tonicidade muscular e trazer flexibilidade e equilíbrio para o seu corpo.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Thiago-Freitas.png');"></div>
<div style="width: 260px;" class="professor-nome">Thiago Freitas<span class="professor-detail">Responsável pelo setor de Fitness, planejamento de aulas coletivas, periodizações e musculação.</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0015_GA.jpg');"></div>
</div>
<div class="rsc_zebra even barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Ginástica Localizada</h3>
<p>Não é à toa que essa atividade não sai da moda: a ginástica localizada conta com uma variedade de exercícios simples de executar. De forma rítmica e utilizando halteres, caneleiras e até mesmo o peso do corpo, aumente a força e a flexibilidade do seu corpo.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Thiago-Freitas.png');"></div>
<div style="width: 260px;" class="professor-nome">Thiago Freitas<span class="professor-detail">Responsável pelo setor de Fitness, planejamento de aulas coletivas, periodizações e musculação.</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0014_localizada.jpg');"></div>
</div>
<div class="rsc_zebra odd barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Step</h3>
<p>Através de uma plataforma ajustável, faça movimentos de subida e descida e trabalhe a resistência do corpo e também a coordenação motora. Um programa de exercício cardiovasculares e de alto gasto calórico.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Thiago-Freitas.png');"></div>
<div style="width: 260px;" class="professor-nome">Thiago Freitas<span class="professor-detail">Responsável pelo setor de Fitness, planejamento de aulas coletivas, periodizações e musculação.</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0013_Step.jpg');"></div>
</div>
<div class="rsc_zebra even barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Stiletto</h3>
<p>Essa atividade é para quem quer se exercitar dançando. O Stilleto mistura jazz com hip hop e ensina movimentos leves e sutis com as mãos, pés, ombros e quadris, jogadas com cabelo, além de olhares sensuais e marcantes.
Esse estilo de dança conta com uma performance única e aprimora as formas do corpo. Além disso, melhora a postura e o equilíbrio e promove um trabalho muscular para todo o corpo de forma muito divertida.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Thalita-Malafaia-.png');"></div>
<div style="width: 260px;" class="professor-nome">Thalita Malafaia<span class="professor-detail">Responsável por toda a grade de Dança da Rio Sport Barra, Recreio e Bonsucesso</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0012_Stiletto.jpg');"></div>
</div>
<div class="rsc_zebra odd barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Mix Dance</h3>
<p>Para quem busca uma aula de dança ao som de músicas bem variadas, o Mix Dance estimula a coordenação motora através de irreverentes coreografias.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Thalita-Malafaia-.png');"></div>
<div style="width: 260px;" class="professor-nome">Thalita Malafaia<span class="professor-detail">Responsável por toda a grade de Dança da Rio Sport Barra, Recreio e Bonsucesso</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0026_mix-dance.jpg');"></div>
</div>
<div class="rsc_zebra even barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Zumba</h3>
<p>Dança e ginástica em uma só atividade? Temos! A Zumba é um exercício aeróbico baseado em movimentos latinos e trabalha principalmente os músculos do quadril, glúteos e coxas.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Thalita-Malafaia-.png');"></div>
<div style="width: 260px;" class="professor-nome">Thalita Malafaia<span class="professor-detail">Responsável por toda a grade de Dança da Rio Sport Barra, Recreio e Bonsucesso</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0011_Zumba.jpg');"></div>
</div>
<div class="rsc_zebra odd barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Spinning</h3>
<p>Vamos pedalar ouvindo uma boa música e ainda perder muitas calorias? As aulas de spinning acontecem em um amplo espaço com muitas bicicletas estacionárias que ficam a sua disposição.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Thiago-Freitas.png');"></div>
<div style="width: 260px;" class="professor-nome">Thiago Freitas<span class="professor-detail">Responsável pelo setor de Fitness, planejamento de aulas coletivas, periodizações e musculação.</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0010_spinning.jpg');"></div>
</div>
<div class="rsc_zebra even barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Yoga</h3>
<p>Atividade perfeita para o corpo e a mente, a Yoga une um conjunto de conhecimentos milenares com técnicas de respiração, meditação e postura. Harmonia e corpo em dia.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Thiago-Freitas.png');"></div>
<div style="width: 260px;" class="professor-nome">Thiago Freitas<span class="professor-detail">Responsável pelo setor de Fitness, planejamento de aulas coletivas, periodizações e musculação.</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0018_yoga.jpg');"></div>
</div>
<div class="rsc_zebra odd barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Running</h3>
<p>Comece aos poucos e fique pronto para uma futura maratona. As aulas de running acontecem em esteiras que variam a velocidade e inclinação. O objetivo é simular as corridas de rua ao embalo de ótimas playlists musicais para aumentar o condicionamento aeróbico e emagrecer.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Thiago-Freitas.png');"></div>
<div style="width: 260px;" class="professor-nome">Thiago Freitas<span class="professor-detail">Responsável pelo setor de Fitness, planejamento de aulas coletivas, periodizações e musculação.</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0017_running.jpg');"></div>
</div>
<div class="rsc_zebra even barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Fast Fit</h3>
<p>Pouco tempo para se exercitar, mas muita vontade de emagrecer e melhorar o condicionamento físico? Bem-vindo aos Fast Fit. Uma aula dinâmica com um sistema de exercícios randômicos que trabalha com 5 e 5 estações em conjunto com a esteira. Feche um circuito de 30 minutos e saia da aula renovado.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Thiago-Freitas.png');"></div>
<div style="width: 260px;" class="professor-nome">Thiago Freitas<span class="professor-detail">Responsável pelo setor de Fitness, planejamento de aulas coletivas, periodizações e musculação.</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0009_fast-fit.jpg');"></div>
</div>
<div class="rsc_zebra odd barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Natação</h3>
<p>Nas nossas piscinas você pode aprender as técnicas da natação que geram muitos benefícios cardiorrespiratórios e melhoram a capacidade física. Em 60 minutos de aula, você pode se manter em forma em todas as estações. Afinal, as nossas piscinas semiolímpicas são aquecidas.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Reinaldo-Arcaro.png');"></div>
<div style="width: 260px;" class="professor-nome">Reinaldo Arcaro<span class="professor-detail">Responsável pelo planejamento e organização de todo o quadro de atividade do setor aquático.</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0028_natacao.jpg');"></div>
</div>
<div class="rsc_zebra even barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Vôlei de praia</h3>
<p>Esse esporte atribui vários benefícios: auxilia no emagrecimento, deixa o corpo em forma, aprimora o reflexo, flexibilidade, impulsão, força, concentração, coordenação motora e noção espacial.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Leandro-Martins.png');"></div>
<div style="width: 260px;" class="professor-nome">Leandro Martins<span class="professor-detail">Responsável pelo Vôlei de Praia, coleciona títulos nas diversas categorias do vôlei de praia e já dirigiu atletas que hoje atuam no Circuito Open e nas seleções do Japão e do Catar.</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0008_volei-de-praia.jpg');"></div>
</div>
<div class="rsc_zebra odd barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Futevôlei</h3>
<p>Exercícios que misturam dois esportes que são paixão nacional estão quadras da Rio Sport Center. Com o futevôlei você vai trabalhar com intensidade os músculos da coxa, panturrilha, glúteos, quadríceps e abdômen.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Anderson-aguia.png');"></div>
<div style="width: 260px;" class="professor-nome">Anderson Águia<span class="professor-detail">Responsável pelo programa de futevôlei, atleta da modalidade e disputa vários campeonatos a nível nacional e internacional.</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0007_futevoley.jpg');"></div>
</div>
<div class="rsc_zebra even barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Jiu Jitsu</h3>
<p>Uma das modalidades mais adoradas por quem é apaixonado por artes marciais. Baseada em chaves e torções, é excelente para quem busca melhorar o condicionamento físico em uma aula dinâmica para homens e mulheres.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Thiago-Freitas.png');"></div>
<div style="width: 260px;" class="professor-nome">Thiago Freitas<span class="professor-detail">Responsável pelo setor de Fitness, planejamento de aulas coletivas, periodizações e musculação.</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0027_jiujitsu.jpg');"></div>
</div>
<div class="rsc_zebra odd barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Judô</h3>
<p>Criado e desenvolvido no Japão, o judô sem enraizou pelo mundo e até mesmo nos tatames olímpicos. Hoje, essa antiga modalidade é praticada por milhares de pessoas e é excelente para desenvolver a concentração, disciplina, equilíbrio, força e lateralidade.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Thiago-Freitas.png');"></div>
<div style="width: 260px;" class="professor-nome">Thiago Freitas<span class="professor-detail">Responsável pelo setor de Fitness, planejamento de aulas coletivas, periodizações e musculação.</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/judo.jpg');"></div>
</div>
<div class="rsc_zebra even barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Defesa pessoal</h3>
<p>Importante para a nossa segurança e também para exercitar a velocidade do raciocínio e do corpo, as aulas de defesa pessoal ensinam técnicas de proteção urbana com desenvolvimento de várias atividades.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Thiago-Freitas.png');"></div>
<div style="width: 260px;" class="professor-nome">Thiago Freitas<span class="professor-detail">Responsável pelo setor de Fitness, planejamento de aulas coletivas, periodizações e musculação.</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0006_defesa-pessoal.jpg');"></div>
</div>
<div class="rsc_zebra odd barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Muay Thai</h3>
<p>A modalidade é antiga, mas há alguns anos caiu ainda mais na moda por ser, além de uma ótima arte marcial, também uma modalidade onde a perda de peso aparece com rapidez. O treino de Muay Thai oferece diferentes formas de socos, chutes, joelhadas, cotoveladas e domínio do adversário. Em um treino agitado e que foge da mesmice, você vai sentir na pele a melhora da flexibilidade, os músculos mais definidos e as gordurinhas indo embora a cada chute.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Thiago-Freitas.png');"></div>
<div style="width: 260px;" class="professor-nome">Thiago Freitas<span class="professor-detail">Responsável pelo setor de Fitness, planejamento de aulas coletivas, periodizações e musculação.</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0005_muay-thay.jpg');"></div>
</div>
<div class="rsc_zebra even barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Boxe</h3>
<p>Em um treino de uma hora, você pode perder de 350 a 500 calorias em um treino de boxe. Além disso, a sua resistência e força muscular vão ganhar destaque. Entre tantos benefícios, a dinâmica aula de boxe da Rio Sport Center proporciona um corpo mais forte, ajuda a aumentar a autoconfiança, amplia a percepção corporal e coordenação motora.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Thiago-Freitas.png');"></div>
<div style="width: 260px;" class="professor-nome">Thiago Freitas<span class="professor-detail">Responsável pelo setor de Fitness, planejamento de aulas coletivas, periodizações e musculação.</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0004_boxe.jpg');"></div>
</div>
<div class="rsc_zebra odd barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Hard Circuit / Funcional</h3>
<p>É uma atividade em circuito de alta intensidade que utiliza movimentos do dia-a-dia buscando a melhora do condicionamento físico, da agilidade, resistência, força e emagrecimento com ou sem implementos.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Marcos-Cordeiro.png');"></div>
<div style="width: 260px;" class="professor-nome">Marcos Cordeiro<span class="professor-detail">Responsável pelo Projeto Fênix</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0003_funcional.jpg');"></div>
</div>
<div class="rsc_zebra even barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Jump</h3>
<p>Cama elástica é pra todas as idades. Ainda mais quando o assunto é perda de peso, aumento de massa muscular e definição dos músculos. A nossa aula de Jump aumenta a aptidão cardiovascular através de coreografias variadas e animadas.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Thiago-Freitas.png');"></div>
<div style="width: 260px;" class="professor-nome">Thiago Freitas<span class="professor-detail">Responsável pelo setor de Fitness, planejamento de aulas coletivas, periodizações e musculação.</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0002_Jump.jpg');"></div>
</div>
<div class="rsc_zebra odd barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Workout</h3>
<p>Imagine uma atividade que utiliza os seus movimentos naturais do cotidiano como saltar, correr, empurrar e puxar e ajuda a melhorar o seu condicionamento físico. Esse é o workout. Ganhe agilidade, força, resistência, coordenação motora, melhore a capacidade respiratória e ainda emagreça.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Marcos-Cordeiro.png');"></div>
<div style="width: 260px;" class="professor-nome">Marcos Cordeiro<span class="professor-detail">Responsável pelo Projeto Fênix</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0001_workout.jpg');"></div>
</div>
<div class="rsc_zebra even barra-da-tijuca no-cats">
<div class="rsc_zebra_content">
<div class="rsc_zebra_info">
<h3>Forró</h3>
<p>Nas aulas de forró da Rio Sport Center, você vai cair no ritmo do nordeste e se mexer em uma dança típica que articula todo o corpo. Se ainda não tem um par, não se preocupe. Aqui você vai encontrar muita gente bacana para dançar com você.</p>

</div>
<div class="rsc_zebra_bottom" style="background-image: url('http://www.riosportcenter.com.br/wp-content/themes/kallyas/assets/img/responsavel_bg.png');">
<div class="professor-image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Thalita-Malafaia-.png');"></div>
<div style="width: 260px;" class="professor-nome">Thalita Malafaia<span class="professor-detail">Responsável por toda a grade de Dança da Rio Sport Barra, Recreio e Bonsucesso</span></div>
<a class="professor-horarios" href="http://www.riosportcenter.com.br/horarios">Veja os horários</a>

</div>
</div>
<div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0000_forro.jpg');"></div>
</div>
</div>

<?php get_footer(); ?>