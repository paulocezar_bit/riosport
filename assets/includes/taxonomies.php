<?php


/* ------------------------------------------------------------------------
-- Taxonomia Tipo Atividade -----------------------------------------------
------------------------------------------------------------------------ */


function category_tipoatividades() {
    $labels = array(
        'name' => __( 'Tipos de Atividades'),
        'singular_name' => __( 'Tipo de atividade'),
        'search_items' =>  __( 'Buscar' ),
        'popular_items' => __( 'Mais usados' ),
        'all_items' => __( 'Todos os tipos de atividade' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Adicionar novo' ),
        'update_item' => __( 'Atualizar' ),
        'add_new_item' => __( 'Adicionar novo tipo de atividade' ),
        'new_item_name' => __( 'Novo' )
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'singular_label' => 'Atividades',
        'all_items' => 'Todos os tipos de atividade',
        'query_var' => true,
        'rewrite' => array( 'slug' => 'tipoatividades' )
    );

    register_taxonomy('tipoatividades', array('atividade'), $args);

}

add_action('init', 'category_tipoatividades');


/* ------------------------------------------------------------------------
-- Taxonomia Localizacao --------------------------------------------------
------------------------------------------------------------------------ */


function category_localizacao() {
    $labels = array(
        'name' => __( 'Localizações'),
        'singular_name' => __( 'Localização'),
        'search_items' =>  __( 'Buscar' ),
        'popular_items' => __( 'Mais usados' ),
        'all_items' => __( 'Todas as localidades' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Adicionar novo' ),
        'update_item' => __( 'Atualizar' ),
        'add_new_item' => __( 'Adicionar nova localidade' ),
        'new_item_name' => __( 'Novo' )
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'singular_label' => 'Localizacao',
        'all_items' => 'Todas as localizações',
        'query_var' => true,
        'rewrite' => array( 'slug' => 'localizacao' )
    );

    register_taxonomy('localizacao', array('atividade','plano','evento','unidade'), $args);

}

add_action('init', 'category_localizacao');


/* ------------------------------------------------------------------------
-- Taxonomia Segunda --------------------------------------------------
------------------------------------------------------------------------ */


function category_segunda() {
    $labels = array(
        'name' => __( 'Segunda'),
        'singular_name' => __( 'Segunda'),
        'search_items' =>  __( 'Buscar' ),
        'popular_items' => __( 'Mais usados' ),
        'all_items' => __( 'Todas os horários' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Adicionar novo' ),
        'update_item' => __( 'Atualizar' ),
        'add_new_item' => __( 'Adicionar novo horário' ),
        'new_item_name' => __( 'Novo' )
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'singular_label' => 'Segunda',
        'all_items' => 'Todos os Horários',
        'query_var' => true,
        'rewrite' => array( 'slug' => 'segunda' )
    );

    register_taxonomy('segunda', array('atividade'), $args);

}

add_action('init', 'category_segunda');

/* ------------------------------------------------------------------------
-- Taxonomia Terça --------------------------------------------------
------------------------------------------------------------------------ */


function category_terca() {
    $labels = array(
        'name' => __( 'Terça'),
        'singular_name' => __( 'Terça'),
        'search_items' =>  __( 'Buscar' ),
        'popular_items' => __( 'Mais usados' ),
        'all_items' => __( 'Todas os horários' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Adicionar novo' ),
        'update_item' => __( 'Atualizar' ),
        'add_new_item' => __( 'Adicionar novo horário' ),
        'new_item_name' => __( 'Novo' )
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'singular_label' => 'Terça',
        'all_items' => 'Todos os Horários',
        'query_var' => true,
        'rewrite' => array( 'slug' => 'terca' )
    );

    register_taxonomy('terca', array('atividade'), $args);

}

add_action('init', 'category_terca');

/* ------------------------------------------------------------------------
-- Taxonomia Quarta --------------------------------------------------
------------------------------------------------------------------------ */


function category_quarta() {
    $labels = array(
        'name' => __( 'Quarta'),
        'singular_name' => __( 'Quarta'),
        'search_items' =>  __( 'Buscar' ),
        'popular_items' => __( 'Mais usados' ),
        'all_items' => __( 'Todas os horários' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Adicionar novo' ),
        'update_item' => __( 'Atualizar' ),
        'add_new_item' => __( 'Adicionar novo horário' ),
        'new_item_name' => __( 'Novo' )
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'singular_label' => 'Quarta',
        'all_items' => 'Todos os Horários',
        'query_var' => true,
        'rewrite' => array( 'slug' => 'quarta' )
    );

    register_taxonomy('quarta', array('atividade'), $args);

}

add_action('init', 'category_quarta');

/* ------------------------------------------------------------------------
-- Taxonomia Quinta--------------------------------------------------------
------------------------------------------------------------------------ */


function category_quinta() {
    $labels = array(
        'name' => __( 'Quinta'),
        'singular_name' => __( 'Quinta'),
        'search_items' =>  __( 'Buscar' ),
        'popular_items' => __( 'Mais usados' ),
        'all_items' => __( 'Todas os horários' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Adicionar novo' ),
        'update_item' => __( 'Atualizar' ),
        'add_new_item' => __( 'Adicionar novo horário' ),
        'new_item_name' => __( 'Novo' )
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'singular_label' => 'Quinta',
        'all_items' => 'Todos os Horários',
        'query_var' => true,
        'rewrite' => array( 'slug' => 'quinta' )
    );

    register_taxonomy('quinta', array('atividade'), $args);

}

add_action('init', 'category_quinta');

/* ------------------------------------------------------------------------
-- Taxonomia Sexta--------------------------------------------------------
------------------------------------------------------------------------ */


function category_sexta() {
    $labels = array(
        'name' => __( 'Sexta'),
        'singular_name' => __( 'Sexta'),
        'search_items' =>  __( 'Buscar' ),
        'popular_items' => __( 'Mais usados' ),
        'all_items' => __( 'Todas os horários' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Adicionar novo' ),
        'update_item' => __( 'Atualizar' ),
        'add_new_item' => __( 'Adicionar novo horário' ),
        'new_item_name' => __( 'Novo' )
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'singular_label' => 'Sexta',
        'all_items' => 'Todos os Horários',
        'query_var' => true,
        'rewrite' => array( 'slug' => 'sexta' )
    );

    register_taxonomy('sexta', array('atividade'), $args);

}

add_action('init', 'category_sexta');

/* ------------------------------------------------------------------------
-- Taxonomia Sexta--------------------------------------------------------
------------------------------------------------------------------------ */


function category_sabado() {
    $labels = array(
        'name' => __( 'Sábado'),
        'singular_name' => __( 'Sábado'),
        'search_items' =>  __( 'Buscar' ),
        'popular_items' => __( 'Mais usados' ),
        'all_items' => __( 'Todos os horários' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Adicionar novo' ),
        'update_item' => __( 'Atualizar' ),
        'add_new_item' => __( 'Adicionar novo horário' ),
        'new_item_name' => __( 'Novo' )
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'singular_label' => 'Sábado',
        'all_items' => 'Todos os Horários',
        'query_var' => true,
        'rewrite' => array( 'slug' => 'sabado' )
    );

    register_taxonomy('sabado', array('atividade'), $args);

}

add_action('init', 'category_sabado');
?>