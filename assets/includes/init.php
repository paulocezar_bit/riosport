<?php



/*  

-----------------------------------------------------------

-- Remover Versão WP -------------------------------------- 

-----------------------------------------------------------

*/



function remove_wp_version(){ return ''; }

add_filter('the_generator', 'remove_wp_version');



/*  

-----------------------------------------------------------

-- Adicionar Thumbnail ------------------------------------ 

-----------------------------------------------------------

*/



add_theme_support( 'post-thumbnails',

	array(

		'post', 'page', 'webdoors', 'atividade', 'unidade', 'servico'

	)

);



/*



-----------------------------------------------------------

-- Adicionar Ajax js -------------------------------------- 

-----------------------------------------------------------

*/



function call_ajaxurl() {

	echo '<script type="text/javascript">var ajaxurl = "' . admin_url('admin-ajax.php') . '";</script>';

}



add_action('wp_head', 'call_ajaxurl');



/*

-----------------------------------------------------------

-- Declaração de JS & Script ------------------------------

-----------------------------------------------------------

*/



function theme_enqueue_style() {

	wp_enqueue_style( 'style', get_stylesheet_uri() );

	wp_enqueue_style( 'global.bundle', get_template_directory_uri() . '/assets/css/global.css' );

	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/css/font-awesome/css/font-awesome.min.css' );

	wp_enqueue_style( 'swipercss', get_template_directory_uri() . '/assets/css/swiper.min.css' );

	if (is_page('contato')) wp_enqueue_style( 'page-materiais', get_template_directory_uri() . '/assets/css/contato.css' );

	if (is_page('riosport')) wp_enqueue_style( 'home', get_template_directory_uri() . '/assets/css/home.css' );

	if(is_page('horarios')) wp_enqueue_style( 'horario', get_template_directory_uri().'/assets/css/horarios.css');

}

function theme_enqueue_script() {

    if(is_page('noticias')) wp_enqueue_script('noticias', get_template_directory_uri() . '/assets/js/noticias.js', array('jquery'),false,true);

	wp_localize_script('noticias', 'ajax_object', array('ajax_url' =>

		                                                    admin_url('admin-ajax.php'), 'outro_valor' => 1234));

    if(is_page('atividades')) wp_enqueue_script('atividades', get_template_directory_uri() . '/assets/js/atividades.js', array('jquery'),false,true);

    wp_localize_script('atividades', 'ajax_object', array('ajax_url' =>

        admin_url('admin-ajax.php'), 'outro_valor' => 1234));

    wp_enqueue_script('swiper',get_template_directory_uri().'/assets/js/swiper.js', array('jquery'),false,true);

    wp_enqueue_script('global',get_template_directory_uri().'/assets/js/global.js', array('jquery','swiper'),false,true);

}



add_action( 'wp_enqueue_scripts', 'theme_enqueue_style' );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_script' );



/*

-----------------------------------------------------------

-- Media & File Uploader ----------------------------------

-----------------------------------------------------------

*/



function prfx_image_enqueue() {

	wp_enqueue_media();

	// Registers and enqueues the required javascript.

	wp_register_script( 'meta-box-image', get_template_directory_uri() . '/assets/js/media-uploader.js', array( 'jquery' ) );

	wp_localize_script( 'meta-box-image', 'meta_image',

		array(

			'title' => __( 'Escolha ou faça o upload de uma imagem', 'prfx-textdomain' ),

			'button' => __( 'Usar esta imagem', 'prfx-textdomain' ),

		)

	);

	wp_enqueue_script( 'meta-box-image' );

}

add_action( 'admin_enqueue_scripts', 'prfx_image_enqueue' );



function file_enqueue() {

	wp_enqueue_media();

	// Registers and enqueues the required javascript.

	wp_register_script( 'file-box-image', get_template_directory_uri() . '/assets/js/file-uploader.js', array( 'jquery' ) );

	wp_localize_script( 'file-box-image', 'meta_image',

		array(

			'title' => __( 'Escolha ou faça o upload de um arquivo', 'prfx-textdomain' ),

			'button' => __( 'Usar este arquivo', 'prfx-textdomain' ),

		)

	);

	wp_enqueue_script( 'file-box-image' );

}

add_action( 'admin_enqueue_scripts', 'file_enqueue' );



/*

-----------------------------------------------------------

-- Register menus---- -------------------------------------

-----------------------------------------------------------

*/



add_action( 'after_setup_theme', 'register_my_menus' );



function register_my_menus(){

	register_nav_menus(array(

	    'header-menu'=>'Header Menu',

        'footer-menu'=>'Footer Menu'

    ));

}



/*

-----------------------------------------------------------

-- Incluir Adicionais ------------------------------------- 

-----------------------------------------------------------

*/


include_once 'util.php';

include_once 'custom-posts.php';

include_once 'taxonomies.php';

include_once 'ajxquery.php';

include_once 'queries.php';

include_once 'horarios.php';




?>