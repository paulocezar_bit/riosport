<?php

function get_video_id($url) {
	$pattern_youtube = '/^(?:https?:\/\/)?(?:www\.|m\.)?(?:youtu\.be\/|youtube\.com\/)(?:embed\/|v\/|&v=\/|watch\?v=|watch\?feature=youtu\.be&v=|video\/)?([\w-]{9,12})(?:(?:\?|\&)(?:\w|\d|\D|\W)*)*$/i';

	$pattern_vimeo = '/^(?:https?:\/\/)?(?:www\.|player\.)?(?:vimeo\.com\/)(?:video\/)?([\w-]{4,12})(?:(?:\?|\&)(?:\w|\d|\D|\W)*)*$/i';

	$res = array();
	if(preg_match('/^(?:https?:\/\/)+(?:\w*\.)?(youtu\.be|youtube)/i', $url)){
		preg_match($pattern_youtube, $url, $match);
		if(count($match)){
			$res['status'] = '200';
			$res['type'] = 'youtube';
			$res['ID'] = $match[1];
			$res['embed'] = 'https://www.youtube.com/embed/' . $match[1] . '?ecver=2';
		}
	}
	elseif(preg_match('/^(?:https?:\/\/)+(?:\w*\.)?(vimeo)/i', $url)){
		preg_match($pattern_vimeo, $url, $match);
		if(count($match)){
			$res['status'] = '200';
			$res['type'] = 'vimeo';
			$res['ID'] = $match[1];
			$res['embed'] = 'https://player.vimeo.com/video/' . $match[1] . '?color=ff0179';
		}
	}
	else{
		$res['status'] = '404';
		$res['type'] = 'Not a valid embeddable video';
	}
	return $res;
}
function truncate_text($string, $limit, $pad = "...", $break = " ") {
	if (strlen($string) <= $limit) return $string;
	if (false !== ($max = strpos($string, $break, $limit))) {
		if ($max < strlen($string) - 1) {
			$string = substr($string, 0, $max) . $pad;
		}
	}
	return $string;
}

    function getPagesNamesSlugs(){

        $pages = get_pages();



        foreach($pages as $page){

            $result[]=array(

                'nome'=> $page->post_title,

                'slug'=> $page->post_name

            );

        }



        return $result;

    }





?>