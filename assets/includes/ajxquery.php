<?php 





/* ------------------------------------------------------------------------

-- Noticias ---------------------------------------------------------------

------------------------------------------------------------------------ */



function get_news_posts(){

	$paged = $_GET['paged'] ;



	$posts = get_posts(array(

		'post_type'      => 'post',

		'post_status'    => 'publish',

		'posts_per_page' => 5,

		'paged'			 => $paged

	));

	

	$html = '';

	foreach($posts as $item){
		
				$regions = get_the_terms($item->ID, 'regiao');
		
				$meta = get_post_meta($item->ID);
		
				$img = get_the_post_thumbnail_url($item->ID, 'full');
		
				$date = get_the_date("m/d/Y",$item->ID);
		
				$html .= '<div class="post rsc_card">'.
				
								 '<div class="post-imagem rsc_card_imagem" style="background-image: url(' . $img . ');">' .
				
								 '</div>' .
				
								 '<div class="post-info rsc_card_info">' .
		
								 '<p class="rsc_card_date">' . $date . '</p>' .
				
								 '<h3 class="rsc_card_title">' . $item->post_title . '</h3>' .
								 
								 '<p class="rsc_card_text">' . truncate_text(wp_strip_all_tags($item->post_content), 80) . '</p>' .
		
								 '<a href="' . get_permalink($item->ID) . '" class="button-orange">Veja mais</a>' .
				
								 '</div>' .
								 
								 '</div>';
		
				$count++;
		
			}
		
			wp_send_json($html);

}



add_action('wp_ajax_get_news_posts', 'get_news_posts');

add_action('wp_ajax_nopriv_get_news_posts', 'get_news_posts');





/* ------------------------------------------------------------------------

-- Atividades -------------------------------------------------------------

------------------------------------------------------------------------ */



function get_activities_posts(){

	$region = $_GET['region'] ;

	$activities = $_GET['activities'] ;

	$paged = $_GET['paged'] ;

	if ($region == 'todos' && $activities == "todas") {

		$activities = get_posts(array(

			'post_type'      => 'atividade',

			'post_status'    => 'publish',

			'posts_per_page' => -1,

			'paged'			 => $paged,

		));

	} else if ($region !="todos" && $activities == "todas") {

		$activities = get_posts(array(

			'post_type'      => 'atividade',

			'post_status'    => 'publish',

			'posts_per_page' => -1,

			'paged'			 => $paged,

			/*'meta_query' => array(

			    array(

			     'key' => 'wpcf-unidade-escolha',

			     'value' => $region

			    )

			)*/

		));

	}else if ($region =="todos" && $activities != "todas") {

		$activities = get_posts(array(

			'post_type'      => 'atividade',

			'post_status'    => 'publish',

			'posts_per_page' => -1,

			'paged'			 => $paged,

			'cat' => $activities,

		));

	} else { // $region !="todos" && $activities != "todas"

		$activities = get_posts(array(

			'post_type'      => 'atividade',

			'post_status'    => 'publish',

			'posts_per_page' => -1,

			'paged'			 => $paged,

			'cat' => $activities,

			/*'meta_query' => array(

			    array(

			     'key' => 'wpcf-unidade-escolha',

			     'value' => $region

			    )

			)*/

		));

	}

	$count = 1;

	$html = '';

	foreach($activities as $item){

		$regions = get_the_terms($item->ID, 'regiao');

		$cats = get_the_terms($item->ID, 'category');

		$meta = get_post_meta($item->ID);

		$img = get_the_post_thumbnail_url($item->ID, 'full');

		$html .= '<div class="rsc_zebra ' . (($count % 2 === 0) ? 'even' : 'odd'). ' ' . $meta['wpcf-unidade-escolha'][0] . '">' .

				 '<div class="rsc_zebra_content">' .

				 '<div class="rsc_zebra_info"><h3>' . $item->post_title . '</h3>' .

				 '<p>' . $meta['wpcf-atividade-descricao'][0] . '</p></div>' .

				 '<div class="rsc_zebra_bottom" style="background-image: url(' . get_template_directory_uri() . '/assets/img/responsavel_bg.png);">' .

				 '<div class="professor-image" style="background-image: url(' . $meta['wpcf-atividade-foto-prof'][0] . ')"></div>' .

				 '<div class="professor-nome">' . $meta['wpcf-atividade-professor'][0] .

				 ' <span class="professor-detail">' . $meta['wpcf-atividade-prof-desc'][0] . '</span>' .

				 '</div>' .

				 '<a href="#" class="professor-horarios">Veja os horários</a>' .

				 '</div>' .

				 '</div>' .

				 '<div class="rsc_zebra_image" style="background-image: url(' . $img . ');">' .

				 '</div>' .

				 '<div data-region="' . $regions .'" style="display: none;"></div>' .

				 '</div>';

		$count++;

	}

	wp_send_json($html);

}



add_action('wp_ajax_get_activities_posts', 'get_activities_posts');

add_action('wp_ajax_nopriv_get_activities_posts', 'get_activities_posts');





/* ------------------------------------------------------------------------

-- Planos -----------------------------------------------------------------

------------------------------------------------------------------------ */



function get_plans_posts(){

	$region = $_GET['region'] ;

	$paged = $_GET['paged'] ;

	if ($region == 'todos') {

		$plans = get_posts(array(

			'post_type'      => 'plano',

			'post_status'    => 'publish',

			'posts_per_page' => -1,

			'paged'			 => $paged

		));

	} else {

		$plans = get_posts(array(

			'post_type'      => 'plano',

			'post_status'    => 'publish',

			'posts_per_page' => -1,

			'paged'			 => $paged,

			/*'meta_query' => array(

			    array(

			     'key' => 'wpcf-unidade-escolha',

			     'value' => $region

			    )

			)*/

		));

	}

	$html = '';

	$count = 1;

	foreach($plans as $item){

		$regions = get_the_terms($item->ID, 'regiao');

		$meta = get_post_meta($item->ID);

		$img = get_the_post_thumbnail_url($item->ID, 'full');



		$html .= '<div class="plano">'.

				 '<div class="plano-imagem" style="background-image: url(' . $img . ');">' .

		         '</div>' .

		         '<div class="plano-info">' .

		         '<h3>' . $item->post_title . '</h3>' .

		         /*'<p>' . $meta['wpcf-plano-desc'][0] . '</p>' .

		         '<a href="' . get_site_url() . '/contato" class="button-orange">Matricule-se</a>' .*/

		         '</div>' .

		         '</div>';

		$count++;

	}

	wp_send_json($html);

}



add_action('wp_ajax_get_plans_posts', 'get_plans_posts');

add_action('wp_ajax_nopriv_get_plans_posts', 'get_plans_posts');





/* ------------------------------------------------------------------------

-- Serviços ---------------------------------------------------------------

------------------------------------------------------------------------ */



function get_service_posts(){

	$region = $_GET['region'] ;

	$paged = $_GET['paged'] ;

	if ($region == 'todos') {

		$services = get_posts(array(

			'post_type'      => 'servico',

			'post_status'    => 'publish',

			'posts_per_page' => -1,

			'paged'			 => $paged

		));

	} else {

		$services = get_posts(array(

			'post_type'      => 'servico',

			'post_status'    => 'publish',

			'posts_per_page' => -1,

			'paged'			 => $paged,

			/*'meta_query' => array(

			    array(

			     'key' => 'wpcf-unidade-escolha',

			     'value' => $region

			    )

			)*/

		));

	}

	$html = '';

	$count = 1;

	foreach($services as $item){

		$regions = get_the_terms($item->ID, 'regiao');

		$meta = get_post_meta($item->ID);

		$img = get_the_post_thumbnail_url($item->ID, 'full');



		$html .= '<div class="plano">'.

				 '<div class="plano-imagem" style="background-image: url(' . $img . ');">' .

		         '</div>' .

		         '<div class="plano-info">' .

		         '<h3>' . $item->post_title . '</h3>' .

		         /*'<p>' . $meta['wpcf-plano-desc'][0] . '</p>' .

		         '<a href="' . get_site_url() . '/contato" class="button-orange">Matricule-se</a>' .*/

		         '</div>' .

		         '</div>';

		$count++;

	}

	wp_send_json($html);

}



add_action('wp_ajax_get_services_posts', 'get_service_posts');

add_action('wp_ajax_nopriv_get_services_posts', 'get_service_posts');



/* ------------------------------------------------------------------------

-- Eventos ----------------------------------------------------------------

------------------------------------------------------------------------ */



function get_events_posts(){

	$paged = $_GET['paged'] ;

	$future = $_GET['future'] === 'true' ? true : false ;

	if($future == true) {

		$events = get_posts(array(

			'post_type'      => 'evento',

			'post_status'    => 'future',

			'posts_per_page' => -1

		));

	} else {

		$events = get_posts(array(

			'post_type'      => 'evento',

			'post_status'    => 'publish',

			'posts_per_page' => 5,

			'paged'			 => $paged

		));

	}

	$html = '';

	foreach($events as $item){

		$meta = get_post_meta($item->ID);

		$img = get_the_post_thumbnail_url($item->ID, 'full');



		$html .= '<div class="event">'.

				 '<div class="event-imagem" style="background-image: url(' . $img . ');">' .

		         '</div>' .

		         '<div class="event-info">' .

		         '<h3>' . $item->post_title . '</h3>' .

		         '</div>' ;

		         if($future) {

		$html .='<div>1</div>';

		         } else {

		$html .='<div>2</div>';

		         }

		$html .= '</div>';

	}

	wp_send_json($html);

}



add_action('wp_ajax_get_events_posts', 'get_events_posts');

add_action('wp_ajax_nopriv_get_events_posts', 'get_events_posts');



?>