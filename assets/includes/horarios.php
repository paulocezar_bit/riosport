<?php

function getHorarios(){

    $_GET['unit'] ? $unidade = $_GET['unit'] : $unidade = 'barra-da-tijuca';

    $horarios = get_posts(array(
        'post_type'=>'atividade',
        'post_status'=>'publish',
        'tax_query' => array(
            array(
                'taxonomy' => 'localizacao',
                'field'    => 'slug',
                'terms'    => $unidade
            ),
        ),
        'posts_per_page'=> -1
    ));

    echo '<script> console.log('.json_encode($horarios).')</script>';

    function getClasses($id){
        $classes['segunda'] = wp_get_post_terms($id,'segunda',array('fields'=>'names'));
        $classes['terça'] = wp_get_post_terms($id,'terca',array('fields'=>'names'));
        $classes['quarta'] = wp_get_post_terms($id,'quarta',array('fields'=>'names'));
        $classes['quinta'] = wp_get_post_terms($id,'quinta',array('fields'=>'names'));
        $classes['sexta'] = wp_get_post_terms($id,'sexta',array('fields'=>'names'));
        $classes['sabado'] = wp_get_post_terms($id,'sabado',array('fields'=>'names'));
        return $classes;
    };

    foreach($horarios as $value){
        $catSlugs = '';
        $categorias = get_the_terms($value -> ID,'tipoatividades');

        foreach ($categorias as $categoria){
            $catSlugs[] = $categoria -> slug;
        }

        $atividades[] = array(
            'atividade'=>$value -> post_title,
            'professor' => get_post_meta($value -> ID,'professor_responsavel',true),
            'horarios' => getClasses($value -> ID),
            'categoria' => $catSlugs
        );
    }



    echo '<script> console.log('.json_encode($atividades).')</script>';



    function buildTable($atividades,$hora, $dia){

        foreach($atividades as $atividade){

            if($atividade['horarios'][$dia]){

                foreach($atividade['horarios'][$dia] as $key){

                    if($key === $hora){

                        echo '<div class="item-atividade" data-categoria="';

                        foreach ($atividade['categoria'] as $datacat){

                            echo $datacat.' ';

                        }

                        echo '">';

                        echo '<p class="atividade-name">'.$atividade['atividade'].'</p>';

                        echo '<p class="atividade-prof">Professor responsável:<br>'.$atividade['professor'].'</p>';

                        echo '</div>';

                    }

                }

            };

        }

    }



    $dias = array('segunda','terça','quarta','quinta','sexta','sabado');

    $horas = array('06:00','06:30','07:00','07:30','08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:30','20:00','20:30','21:00','21:30','22:00');



    //Unit select



    $unidadesList = get_posts(array(
        'post_type' => 'unidade',
        'post_status' => 'publish',
        'orderby' => 'post_title',
        'order' => 'ASC',
        'posts_per_page'=>-1
    ));



    //echo '<script> console.log('.json_encode($unidadesList).')</script>';


    echo '<form id="unidade-form" method="GET" action="/horarios/"><select name="unit" id="unidade-select">';

    foreach($unidadesList as $singleunidade){

        echo '<option value="'.$singleunidade->post_name.'" '.selected($unidade,$singleunidade->post_name,false).'>'.$singleunidade->post_title.'</option>';

    }

    echo '</select>';



    //Category select



    echo '<select name="category" id="category-select">';

    $listcategories = get_terms(array(
        'taxonomy'=>'tipoatividades'
    ));

    //echo '<script> console.log('.json_encode($listcategories).')</script>';

    echo '<option value="" selected disabled hidden>Atividade</option>';

    echo '<option value="all">Todas</option>';

    foreach($listcategories as $singlecategory){

        echo '<option value="'.$singlecategory->slug.'">'.$singlecategory->name.'</option>';

    }

    echo '</select>';

    echo '</form>';



    //Build table



    echo '

        <table>

            <tr>

                <th>Horários</th>

                <th>Seg</th>

                <th>Ter</th>

                <th>Qua</th>

                <th>Qui</th>

                <th>Sex</th>
                
                <th>Sab</th>

            </tr>'

    ;



    foreach($horas as $hora){

        echo '<tr>';

        echo '<td>' . $hora . '</td>';

        foreach($dias as $dia){

            echo '<td>';

            if($atividades){
                buildTable($atividades,$hora,$dia);
            }

            echo '</td>';

        }

        echo '</tr>';

    }



    echo '</table>';



    echo "<script>var buscaUnidade = {



        search: function () {



            jQuery('#unidade-select').on('change', function(e) {

                e.preventDefault();

                jQuery('#unidade-form').submit();                

            });



        },

        activityFilter: function(){

            jQuery('#category-select').on('change',function(){

               var query = jQuery(this).val();

               console.log(query);

               if(query === 'all'){

                   jQuery('.item-atividade').show();

               } else {

                   query = '.item-atividade[data-categoria~=' +query+ ']';

                   console.log(query);

                   jQuery('.item-atividade').hide();

                   jQuery(query).show();

               }

            });

        },

        init: function(){

            buscaUnidade.search();

            buscaUnidade.activityFilter();

        }

    }



    jQuery(document).ready(function(){

        console.log('Mapa de Impacto');

        buscaUnidade.init();



    });</script>";

} ?>