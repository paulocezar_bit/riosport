var jsGlobal = {
	webdoor: function($){
		var webdoorSwiper = new Swiper('#webdoorSwiper',{
			autoplay: '5000',
			effect: 'fade',
            nextButton: '#webdoor-swiper-next',
            prevButton: '#webdoor-swiper-prev'
		});
	},
	init: function($){
		jsGlobal.webdoor($);
	}
};

jQuery(document).ready(function() {
	jsGlobal.init($);
});