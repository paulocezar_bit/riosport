var Eventos = {

    getNews: function(){
        jQuery.get(ajax_object.ajax_url, { action:"get_events_posts", 'future':'true' },function(response){
            jQuery('.js-getnew').append(response);
        });
        jQuery.get(ajax_object.ajax_url, { action:"get_events_posts" },function(response){
            jQuery('.js-getpast').append(response);
        });
    },

    loadMore: function(){
        jQuery('#load-more').click(function(e){
            e.preventDefault();
            var pagecount = parseInt(jQuery('.js-getpast').data('page'))+1;
            console.log(pagecount);
            jQuery.get(ajax_object.ajax_url, { action:"get_events_posts", 'paged': pagecount },function(response){
                if(response){
                    jQuery('.js-getpast').append(response);
                    jQuery('.js-getpast').data('page',pagecount);
                } else {
                    jQuery('#load-more').hide();
                }
            });
        });
    },

    init: function($){
      Eventos.getNews();
      Eventos.loadMore();
    }
}

jQuery(document).ready(function(){
    Eventos.init();
});