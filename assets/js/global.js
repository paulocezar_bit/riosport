var jsGlobal = {

    webdoor: function($) {

        var webdoorSwiper = new Swiper('#webdoorSwiper', {

            autoplay: '5000',

            effect: 'fade',

            nextButton: '.swiper-button-next',

            prevButton: '.swiper-button-prev'

        });

    },

    setPlano: function() {
        jQuery('.choose').on('click', function() {
            var _this = jQuery(this);
            if (jQuery(this).hasClass('active')) {
                return false;
            } else {
                jQuery('.choose').removeClass('active');
                jQuery(this).addClass('active');
                jQuery('.planos_wrapper.active').fadeOut(300, function() {
                    jQuery(this).removeClass('active');
                    jQuery('#planos_' + _this.attr('data-plan')).fadeIn(300, function() {
                        jQuery(this).addClass('active');
                    });
                });
            }
        });
    },

    init: function($) {

        jsGlobal.webdoor($);
        jsGlobal.setPlano();

    }

};



jQuery(document).ready(function() {

    jsGlobal.init($);

});