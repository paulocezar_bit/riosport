var Noticias = {

    getNews: function($){
        $.get(ajax_object.ajax_url, { action:"get_news_posts" },function(response){
            $('#news-list').append(response);
        });
    },

    loadMore: function($){
        $('#load-more').click(function(ev){
            ev.preventDefault();
            var pagecount = parseInt($('#news-list').data('page'))+1;
            console.log(pagecount);
            $.get(ajax_object.ajax_url, { action:"get_news_posts", 'paged': pagecount },function(response){
                if(response){
                    $('#news-list').append(response);
                    $('#news-list').data('page',pagecount);
                } else {
                    $('#load-more').hide();
                }
            });
        });
    },

    init: function($){
      Noticias.getNews($);
      Noticias.loadMore($);
    }
}

jQuery(document).ready(function($){
    Noticias.init($);
});