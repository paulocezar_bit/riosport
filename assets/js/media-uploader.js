/**
 * Created by daniel.albino on 08/06/2017.
 */

jQuery(document).ready(function($){

    // Instantiates the variable that holds the media library frame.
    var meta_image_frame;

    // Instantiates the variable that holds the image field ID
    var meta_image_field = '';

    // Runs when the image button is clicked.
    $('.media-upload-button').click(function(ev){

        // Prevents the default action from occuring.
        ev.preventDefault();

        //Saves the current field ID
        meta_image_field = $(this).parent('.media-field-wrapper').attr('id');


        // If the frame already exists, re-open it.
        // if ( meta_image_frame ) {
        //     meta_image_frame.open();
        //     return;
        // }

        // Sets up the media library frame
        meta_image_frame = wp.media.frames.meta_image_frame = wp.media({
            title: meta_image.title,
            button: { text:  meta_image.button },
            library: { type: 'image' }
        });

        // Runs when an image is selected.
        meta_image_frame.on('select', function(){
            // Grabs the attachment selection and creates a JSON representation of the model.
            var media_attachment = meta_image_frame.state().get('selection').first().toJSON();
            console.log(media_attachment.url);
            console.log($('#'+meta_image_field));
            // Sends the attachment URL to our custom image input field.
            $('#'+meta_image_field).find('.selected-image-field').val(media_attachment.url);
            console.log(media_attachment);

            //Generates the Preview Image field with the attachment URL
            $('#'+meta_image_field).find('.uploaded-image img').attr('src',media_attachment.url);
        });

        // Opens the media library frame.
        meta_image_frame.open();
    });

    $('.multiple-media-upload-button').click(function(ev){

        // Prevents the default action from occuring.
        ev.preventDefault();

        //Saves the current field ID
        meta_image_field = $(this).parent('.multiple-media-field-wrapper').attr('id');


        // If the frame already exists, re-open it.
        // if ( meta_image_frame ) {
        //     meta_image_frame.open();
        //     return;
        // }

        // Sets up the media library frame
        meta_image_frame = wp.media.frames.meta_image_frame = wp.media({
            title: meta_image.title,
            button: { text:  meta_image.button },
            library: { type: 'image' },
            multiple: true
        });

        // Runs when an image is selected.
        meta_image_frame.on('select', function(){
            // Grabs the attachment selection and creates a JSON representation of the model.
            var media_attachment = meta_image_frame.state().get('selection').toJSON();
            console.log(media_attachment.url);
            console.log($('#'+meta_image_field));
            // Sends the attachment URL to our custom image input field.
            $('#'+meta_image_field).find('.selected-image-field').val(media_attachment.url);
            console.log(media_attachment);

            //Generates the Preview Image field with the attachment URL
            $('#'+meta_image_field).find('.uploaded-image img').attr('src',media_attachment.url);
        });

        // Opens the media library frame.
        meta_image_frame.open();
    });

    $('.delete-button').click(function(){
        var selectedField = $(this).parent('.media-field-wrapper').attr('id');
        console.log(selectedField);
        $('#'+ selectedField ).find('.selected-image-field').val('');
        $('#'+ selectedField ).find('.uploaded-image img').attr('src','');
    });
});