/**
 * Created by daniel.albino on 08/06/2017.
 */

jQuery(document).ready(function($){

    // Instantiates the variable that holds the media library frame.
    var meta_image_frame;

    // Instantiates the variable that holds the image field ID
    var meta_image_field = '';

    // Runs when the image button is clicked.
    $('.file-upload-button').click(function(ev){

        // Prevents the default action from occuring.
        ev.preventDefault();

        //Saves the current field ID
        meta_image_field = $(this).parent('.file-field-wrapper').attr('id');


        // If the frame already exists, re-open it.
        // if ( meta_image_frame ) {
        //     meta_image_frame.open();
        //     return;
        // }

        // Sets up the media library frame
        meta_image_frame = wp.media.frames.meta_image_frame = wp.media({
            title: meta_image.title,
            button: { text:  meta_image.button }
        });

        // Runs when an image is selected.
        meta_image_frame.on('select', function(){
            // Grabs the attachment selection and creates a JSON representation of the model.
            var media_attachment = meta_image_frame.state().get('selection').first().toJSON();
            // Sends the attachment URL to our custom image input field.
            $('#'+ meta_image_field ).find('.selected-file-field').val(media_attachment.url);
        });

        // Opens the media library frame.
        meta_image_frame.open();
    });

    $('.delete-button').click(function(){
        var selectedField = $(this).parent('.file-field-wrapper').attr('id');
        $('#'+ selectedField ).find('.selected-file-field').val('');
    });
});