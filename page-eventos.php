<?php get_header(); ?>



<div class="gridlock" >

    <h2 class="rsc_title">Eventos Futuros</h2>
    <div class="list-events js-getnew">

        <div class="rsc_zebra event odd" data-id="01">
            <div class="rsc_zebra_content">
                <div class="rsc_zebra_info">
                    <h3><a href="#">Espetáculo de Dança - Mágico de Oz</a></h3>
                    <p>Que tal começar a se preparar para o espetáculo de dança de final de ano, hein?

Esse ano o musical será inspirado no Mágico e as inscrições vão até 22 de setembro na Rio Sport.

Venha fazer parte desse momento lindo e corra antes que as inscrições acabam, ok?</p>
                    <p class="info_date">26/11 - 16:30h</p>
                    <p class="info_place">Teatro Antônio Fagundes - Av. Ayrton Senna 2541</p>
                </div>
                <div class="rsc_zebra_bottom">
                    <!--<p>Compartilhe</p>
                    <ul class="sociallinks">
                        <li><a href="https://www.facebook.com/sharer/sharer.php?u=%23123" class="facebook">Facebook</a></li>
                        <li><a href="https://twitter.com/home?status=%23123" class="twitter">Twitter</a></li>
                    </ul>-->
                </div>
            </div>
            <div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/020587f81e1aa2e8526dd32e77aea31f.jpg');"></div>
        </div>

        <div class="rsc_zebra event even" data-id="01">
            <div class="rsc_zebra_content">
                <div class="rsc_zebra_info">
                    <h3><a href="#">Meia do Pontal</a></h3>
                    <p>No dia 30/09 teremos uma corrida noturna super divertida que será dividida da seguinte forma:

São duas modalidades:
5k - inscrições R$120
21K - inscrições R$150

*Ambas inscrições incluem camiseta e bolsa.

Gostou? Bora começar os treinos com meta marcada?

Entao corre até o dia 13/09 para o SAC de qualquer RioSport para fechar a sua inscrição</p>
                    <p class="info_date">30/09 - 19h</p>
                    <p class="info_place">Praia do Recreio - Pontal</p>
                </div>
                <div class="rsc_zebra_bottom">
                    <!--<p>Compartilhe</p>
                    <ul class="sociallinks">
                        <li><a href="https://www.facebook.com/sharer/sharer.php?u=%23123" class="facebook">Facebook</a></li>
                        <li><a href="https://twitter.com/home?status=%23123" class="twitter">Twitter</a></li>
                    </ul>-->
                </div>
            </div>
            <div class="rsc_zebra_image" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/21272324_1402396246462298_3652906575952587648_n.jpg');"></div>
        </div>

    </div>

</div>

<div class="noticias-wrapper gridlock">
    <h2 class="rsc_title">Eventos Passados</h2>
    <div class="noticias list-events js-getpast" data-page="1">

        <div class="rsc_card odd" data-id="01">
            <div class="rsc_card_imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/08/trilha.png');"></div>
            <div class="rsc_card_info">
                <p class="rsc_card_date">27/08 - 7h</p>
                <h3 class="rsc_card_title">Trilha Pedra Bonita</h3>
                <p class="rsc_card_text">No dia 27/08 às 7h da manhã fizemos um programa diferente, uma trilha super animada na Pedra Bonita do RJ.</p>
                <a id="load-more" class="button-orange" href="https://www.facebook.com/events/112304682804383/">Veja mais</a>
            </div>
        </div>

        <div class="rsc_card even" data-id="01">
            <div class="rsc_card_imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/1.572895.jpg');"></div>
            <div class="rsc_card_info">
                <p class="rsc_card_date">19/08 - 11h</p>
                <h3 class="rsc_card_title">Bateria da Mocidade - Arena Rio Sport</h3>
                <p class="rsc_card_text">A Agremiação de Padre Miguel, se apresentou na abertura da terceira etapa do Campeonato Nacional de Futevôlei.</p>
                <a id="load-more" class="button-orange" href="https://www.facebook.com/events/707867616073972/">Veja mais</a>
            </div>
        </div>

        <div class="rsc_card odd" data-id="01">
            <div class="rsc_card_imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/20017695_1353380218030568_3405507895338764813_o.png');"></div>
            <div class="rsc_card_info">
                <p class="rsc_card_date">28/07 - 9h</p>
                <h3 class="rsc_card_title">Arraiá Rio Sport - O maior arraiá da Barra</h3>
                <p class="rsc_card_text">Nos dias 28,29 e 30 de julho a Rio Sport Barra rolou o maior arraiá da Barra da Tijuca.</p>
                <a id="load-more" class="button-orange" href="https://www.facebook.com/events/153372651887686/">Veja mais</a>
            </div>
        </div>

    </div>
    <!--<a id="load-more" class="button-orange" href="https://www.facebook.com/events/112304682804383/">Veja mais</a>-->
</div>



<?php get_footer(); ?>