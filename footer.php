	



        <footer>

            <div class="gridlock">

                <div class="left">

                    <h2>Navegação</h2>

                    <?php wp_nav_menu(array(

                        'theme_location' => 'footer-menu',

                        'container_class' => 'menu'

                    )); ?>

                </div><div class="central">

                    <img src="<?php echo get_template_directory_uri() . '/assets/img/logoriosport.png' ?>" alt="RioSport">

                    <div class="social">

                        <a href="https://www.facebook.com/riosportacademia/"><i class="fa fa-facebook" aria-hidden="true"></i></a>

                        <a href="https://www.instagram.com/riosportacademia/"><i class="fa fa-instagram" aria-hidden="true"></i></a>

                    </div>

                    <small>Copyright © 2017 Rio Sport Center</small>

                </div><div class="right">

                    <h2>Horários</h2>

                    <p><b>Segunda-Sexta</b> 6h-23h</p>

                    <p><b>Sábado</b> 9h-16h</p>

                    <h2>Email</h2>

                    <a href="mailto:contato@riosportcenter.com.br">contato@riosportcenter.com.br</a>

                    <h2>Telefone</h2>

                    <p><b>Barra da Tijuca</b> (21) 3325-6644</p>

                    <p><b>Bonsucesso</b> (21) 2497-2020</p>

                    <p><b>Recreio</b> (21) 3591-1905</p>

                </div>

            </div>

        </footer>

        <?php wp_footer(); ?>

    </body>

</html>