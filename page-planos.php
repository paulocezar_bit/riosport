<?php get_header();
?>

<ul class="planos-chooser">
    <li class="choose active" data-plan="barra">Barra da Tijuca</li>
    <li class="choose" data-plan="recreio">Recreio</li>
    <li class="choose" data-plan="bonsucesso">Bonsucesso</li>
</ul>

<div class="gridlock planos">

<div id="planos_barra" class="planos_wrapper active">
    <div class="plano odd">
        <div class="plano-imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0028_natacao.jpg');"></div>
        <div class="plano-info" style="margin-bottom: 20px;">
            <h3>COMBO WATER</h3>
            <p style="height: 176px;">Inclui toda a parte aquática da Rio Sport: Natação, Hidroginástica, sauna e afins. A partir de: R$ 239,00</p>
            <a href="/contato" class="button-orange">Matricule-se</a>
        </div>
    </div>

    <div class="plano even">
        <div class="plano-imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/judo.jpg');"></div>
        <div class="plano-info" style="margin-bottom: 20px;">
            <h3>COMBO FIGHT</h3>
            <p style="height: 176px;">Inclui todas as lutas oferecidas pela academia no espaço Fight Club: Defesa Pessoal, Jiu Jitsu, Boxe, Fight Fitness, Judô e
Muay Thai. A partir de: R$ 299,00</p>
            <a href="/contato" class="button-orange">Matricule-se</a>
        </div>
    </div>

    <div class="plano odd">
        <div class="plano-imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/07/planos_one.jpg');"></div>
        <div class="plano-info" style="margin-bottom: 20px;">
            <h3>COMBO FITNESS</h3>
            <p style="height: 176px;">Inclui: Localizada, GA, Alongamento, Mix Dance, Rítmos, Step, Localizada, Abdominal Express, Zumba, Yoga, GAP, Spinning, Thai Boxe, Power Ballet, Running, Fast Fit, Cross Fit, Fênix, Fênix Mobility, Fênix Performance e Musculação. A partir de: R$ 239,00</p>
            <a href="/contato" class="button-orange">Matricule-se</a>
        </div>
    </div>

<div class="plano odd">
        <div class="plano-imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/07/planos_mais.jpg');"></div>
        <div class="plano-info" style="margin-bottom: 20px;">
            <h3>RIO SPORT MAIS</h3>
            <p style="height: 176px;">Dá direito a utilizar todas as modalidades oferecidas pela academia, inclusive as que não estão em nenhum dos planos acima como vôley, futevôley, pilates, tênis e beach tennis. A partir de: R$ 449,00
</p>
            <a href="/contato" class="button-orange">Matricule-se</a>
        </div>
    </div>

    <div class="plano even">
        <div class="plano-imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/barra-07.jpg');"></div>
        <div class="plano-info" style="margin-bottom: 20px;">
            <h3>PLANO HIDRO</h3>
            <p style="height: 176px;"> - A partir de: R$ 280,00</p>
            <a href="/contato" class="button-orange">Matricule-se</a>
        </div>
    </div>

    <div class="plano odd">
        <div class="plano-imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/07/planos_kids.jpg');"></div>
        <div class="plano-info" style="margin-bottom: 20px;">
            <h3>PLANO KIDS</h3>
            <p style="height: 176px;"> - A partir de: R$ 440,00</p>
            <a href="/contato" class="button-orange">Matricule-se</a>
        </div>
    </div>


<div class="plano odd">
        <div class="plano-imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/Baby-swimming-in-the-pool.jpg');"></div>
        <div class="plano-info" style="margin-bottom: 20px;">
            <h3>PLANO BABY</h3>
            <p style="height: 176px;"> - A partir de: R$ 489,00</p>
            <a href="/contato" class="button-orange">Matricule-se</a>
        </div>
    </div>

    <div class="plano even">
        <div class="plano-imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/rs_0019_Ginastica-artistica.jpg');"></div>
        <div class="plano-info" style="margin-bottom: 20px;">
            <h3>PLANO TEENS</h3>
            <p style="height: 176px;">  - A partir de: R$ 404,00</p>
            <a href="/contato" class="button-orange">Matricule-se</a>
        </div>
    </div>

    <div class="plano odd">
        <div class="plano-imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/pilates.jpg');"></div>
        <div class="plano-info" style="margin-bottom: 20px;">
            <h3>PLANO PILATES</h3>
            <p style="height: 176px;"> - A partir de: R$ 157,50</p>
            <a href="/contato" class="button-orange">Matricule-se</a>
        </div>
    </div>

</div>

<div id="planos_recreio" class="planos_wrapper">
    <div class="plano odd">
        <div class="plano-imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/judo.jpg');"></div>
        <div class="plano-info" style="margin-bottom: 20px;">
            <h3>COMBO FIGHT</h3>
            <p style="height: 176px;">Inclui todas as lutas oferecidas pela academia no espaço Fight Club: Defesa Pessoal, Jiu Jitsu, Boxe, Fight Fitness, Judô e
Muay Thai. A partir de: R$ 269,00</p>
            <a href="/contato" class="button-orange">Matricule-se</a>
        </div>
    </div>

    <div class="plano even">
        <div class="plano-imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/07/planos_one.jpg');"></div>
        <div class="plano-info" style="margin-bottom: 20px;">
            <h3>COMBO FITNESS</h3>
            <p style="height: 176px;">Inclui: Localizada, GA, Alongamento, Mix Dance, Rítmos, Step, Localizada, Abdominal Express, Zumba, Yoga, GAP, Spinning, Thai Boxe, Power Ballet, Running, Fast Fit, Cross Fit, Fênix, Fênix Mobility, Fênix Performance e Musculação. A partir de: R$ 239,00
</p>
            <a href="/contato" class="button-orange">Matricule-se</a>
        </div>
    </div>

    <div class="plano odd">
        <div class="plano-imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/07/planos_mais.jpg');"></div>
        <div class="plano-info" style="margin-bottom: 20px;">
            <h3>RIO SPORT MAIS</h3>
            <p style="height: 176px;">Dá direito a utilizar todas as modalidades oferecidas pela academia, inclusive as que não estão em nenhum dos planos acima como vôley, futevôley, pilates, tênis e beach tennis. A partir de: R$ 199,00
</p>
            <a href="/contato" class="button-orange">Matricule-se</a>
        </div>
    </div>


<div class="plano odd">
        <div class="plano-imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/07/planos_kids.jpg');"></div>
        <div class="plano-info" style="margin-bottom: 20px;">
            <h3>PLANO KIDS</h3>
            <p style="height: 176px;"> - A partir de: R$ 266,00</p>
            <a href="/contato" class="button-orange">Matricule-se</a>
        </div>
    </div>

    <div class="plano even">
        <div class="plano-imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/pilates.jpg');"></div>
        <div class="plano-info" style="margin-bottom: 20px;">
            <h3>PLANO PILATES</h3>
            <p style="height: 176px;">PLANO PILATES - A partir de: R$ 99,00</p>
            <a href="/contato" class="button-orange">Matricule-se</a>
        </div>
    </div>
</div>

<div id="planos_bonsucesso" class="planos_wrapper">
    <div class="plano odd">
        <div class="plano-imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/07/planos_mais.jpg');"></div>
        <div class="plano-info" style="margin-bottom: 20px;">
            <h3>RIO SPORT MAIS</h3>
            <p style="height: 176px;">Dá direito a utilizar todas as modalidades oferecidas pela academia, inclusive as que não estão em nenhum dos planos acima como vôley, futevôley, pilates, tênis e beach tennis. A partir de: R$ 126,00
</p>
            <a href="/contato" class="button-orange">Matricule-se</a>
        </div>
    </div>

    <div class="plano even">
        <div class="plano-imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/descrio-fsica-da-unisuam-bonsucesso-2-638.jpg');"></div>
        <div class="plano-info" style="margin-bottom: 20px;">
            <h3>PLANO UNISUAM</h3>
            <p style="height: 176px;">- A partir de: R$ 99,00</p>
            <a href="/contato" class="button-orange">Matricule-se</a>
        </div>
    </div>

    <div class="plano odd">
        <div class="plano-imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/bonsucesso-03.jpg');"></div>
        <div class="plano-info" style="margin-bottom: 20px;">
            <h3>PLANO ESTUDANTE</h3>
            <p style="height: 176px;"> - A partir de: R$ 89,00</p>
            <a href="/contato" class="button-orange">Matricule-se</a>
        </div>
    </div>
</div>

</div>

<div class="gridlock">
    <h2 class="rsc_title">Vantagens</h2>
    <p class="rsc_text subtitle">Aqui, oferecemos de tudo para o seu corpo e saúde ficarem na medida certa.
</p>

    <div class="rsc_vantagens">
        <div class="rsc_vantagens_img" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/vantagens_0007_01.png')"></div>
        <h4>Atendimento Personalizado</h4>
        <p>Profissionais altamente qualificados para te ajudar sempre que precisar.</p>
    </div>

    <div class="rsc_vantagens">
        <div class="rsc_vantagens_img" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/vantagens_0006_02.png')"></div>
        <h4>Avaliação Nutricional</h4>
        <p>Nutricionistas dentro das unidades pra te ajudar alcançar seus objetivos.</p>
    </div>

    <div class="rsc_vantagens">
        <div class="rsc_vantagens_img" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/vantagens_0005_03.png')"></div>
        <h4>Infraestrutura Completa</h4>
        <p>Ambiente confortável com equipamentos modernos e profissionais altamente qualificados.</p>
    </div>

    <div class="rsc_vantagens">
        <div class="rsc_vantagens_img" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/vantagens_0004_04.png')"></div>
        <h4>Estacionamento Privativo</h4>
        <p>Toda seguraça e comodidade na hora de estacinar seu veículo.</p>
    </div>

    <div class="rsc_vantagens">
        <div class="rsc_vantagens_img" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/vantagens_0003_05.png')"></div>
        <h4>Avaliação Física Periódica</h4>
        <p>Nossos profissionais de educação física estão prontos para te ajudar a ver se está em ordem</p>
    </div>

    <div class="rsc_vantagens">
        <div class="rsc_vantagens_img" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/vantagens_0002_06.png')"></div>
        <h4>Acompanhamento Individual</h4>
        <p>Aqui voc6e encontra sempre um profissional pra te orientar e ajudar no que você precisar.</p>
    </div>

    <div class="rsc_vantagens">
        <div class="rsc_vantagens_img" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/vantagens_0001_07.png')"></div>
        <h4>Atividades Diferenciadas</h4>
        <p>Cada corpo pede uma atividade diferente, pode contar que aqui tem tudo que você precisa.</p>
    </div>

    <div class="rsc_vantagens">
        <div class="rsc_vantagens_img" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/vantagens_0000_08.png')"></div>
        <h4>Aplicativo Personalizado</h4>
        <p>Seus horários, treinos e toda a academia na palna da sua mão.</p>
    </div>

</div>

<?php get_footer();
?>