<?php get_header(); ?>

	<div class="gridlock">
		<section class="latest-posts-wrapper">
			<h2 class="latest-posts-title">Últimas Notícias</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eget magna eu tortor venenatis auctor ut id purus. Quisque tincidunt id enim et laoreet. Nunc nulla ipsum, pellentesque nec metus in, finibus ultrices elit.</p>
			<?php $news = homeGetNews(); ?>
			<?php foreach($news as $new):?>
				<div class="latest-post">
					<div class="latest-post-image" style="background-image: url(<?php echo $new['thumb']; ?>)"></div>
					<h3><?php echo $new['title']; ?></h3>
					<p class="post-info"><span><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $new['date'] ?></span>
						<span><i class="fa fa-comment-o" aria-hidden="true"></i> <?php echo $new['comments'] ?></span></p>
					<p><?php echo $new['excerpt'] ?></p>
				</div>
			<?php endforeach; ?>
		</section>
	</div>

	<!-- EDIT -->
	<div class="section" style="margin: 0 auto;">
		<div class="container" style="width: 100%; height: 1200px;">
			<div class="row" style="height: 100%">

				<div class="col-md-6" style="padding-left:0px; padding-right:0px;">

					<div class="col-md-12" id="box1" style="padding-top:25%; width: 100%; height: 600px; background-image:url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/bg-yellow.png);background-repeat:repeat;background-position:center top;background-size:cover;background-attachment:scroll">
						<div style="height: 151px; width: 400px; margin: 0 auto; align-self: center;">
							<h2 class="text-left text-primary" contenteditable="true">MUITO MAIS QUE
								<br>UMA ACADEMIA.</h2>
							<p class="text-left text-primary">Somos um complexo completo para o seu corpo.
								<br>Piscinas aquecidas, ginásios, salas de spinning e ginástica, beach tennis,
								pilates, fisioterapia e outras inúmeras atividades fazem parte do complexo
								poliesportivo. Tudo para o seu conforto e bem estar. Vem conosco fazer
								parte dessa família.
								<br>
								<br>Bem vindo a Rio Sport.</p>
						</div>
					</div>

					<div class="col-md-12" id="box2" style="padding-top:25%; width: 100%; height: 600px; background-image:url(http://www.riosportcenter.com.br/wp-content/uploads/2017/08/crossfit-2.png);background-repeat:repeat;background-position:center top;background-size:cover;background-attachment:scroll">
						<div style="height: 151px; width: 400px; margin: 0 auto; align-self: center;">
							<h3 class="text-left text-primary">SIM, NÓS TEMOS CROSS FIT</h3>
							<p class="text-left text-primary">O seu corpo é mais resistente do que você imagina.
								<br>Descubra o verdadeiro potencial do seu corpo. Venha experimentar o Crossfit
								Mutuca na Rio Sport.</p>
						</div>
					</div>

				</div>

				<div class="col-md-6" style="padding-left:0px; padding-right:0px;height: 100%">

					<div class="col-md-12" id="box3" style="padding-top:50%; margin-left:0px; margin-right:0px; width: 100%; height: 100%; background-image:url(http://www.riosportcenter.com.br/wp-content/uploads/2017/08/MAIS-2.png);background-repeat:repeat;background-position:center top;background-size:cover;background-attachment:scroll">
						<div style="height: 151px; width: 400px; margin: 0 auto; align-self: center;">
							<h3 class="text-primary text-right" contenteditable="true">DO QUE O SEU CORPO
								<br>PRECISA HOJE?</h3>
							<p class="text-primary text-right">O Rio Sport Mais é perfeito para quem gosta de várias modalidades e quer
								ter em mãos todas as opções para obter a melhor forma física.</p>
						</div>
					</div>

				</div>

			</div>
		</div>
	</div>

	<div class="section" style="position:relative;">

		<div class="container">

			<div class="row">
				<div class="col-md-12">
					<h2 class="text-center">DESTAQUES DA ACADEMIA</h2>
					<hr style="width:200px; background-color:#ff7800; height: 1px; border: 0; margin: 0 auto;">
					<p class="text-center">Confira algumas das atividades e pofissionais que estão em alta na Rio
						Sport.</p>
				</div>
			</div>

			<div class="row" style="padding-left: 50px;">

				<div class="col-md-4" style="width: 350px; height: 600px; background-image:url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/fernando-1.png);background-repeat:repeat;background-position:center top;background-size:cover;background-attachment:scroll">

					<div style="position: absolute; bottom:10px; margin:10; margin-right:30px;">
						<h3 class="text-center text-primary">Fernando Javier</h3>
						<p class="text-center text-primary">Responsável pela organização da grade do Tênis, ministra aulas, organizam
							campeonatos, atividades para o setor Kids e Beach Tênis.</p>
						<div style="margin: 0 auto; width:150px;">
							<a class="btn btn-warning" style="width:150px; background-color:#ff7800">Dias e Horários</a>
						</div>
					</div>

				</div>

				<div class="col-md-4" style="margin-left: 10px; width: 350px; height: 600px; background-image:url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/chris-1.png);background-repeat:repeat;background-position:center top;background-size:cover;background-attachment:scroll">

					<div style="position: absolute; bottom:10px; margin:10; margin-right:30px;">
						<h3 class="text-center text-primary">Chris Hani</h3>
						<p class="text-center text-primary">Professora de Educação Física, exerce a função de comunicação entre os
							clientes e a gestão superior, atua como agente de relacionamento.</p>
						<div style="margin: 0 auto; width:150px;">
							<a class="btn btn-warning" style="width:150px; background-color:#ff7800;">Dias e Horários</a>
						</div>
					</div>

				</div>

				<div class="col-md-4" style="margin-left: 10px; width: 350px; height: 600px; background-image:url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/marcos-1.png);background-repeat:repeat;background-position:center top;background-size:cover;background-attachment:scroll">

					<div style="position: absolute; bottom:10px; margin:10; margin-right:30px;">
						<h3 class="text-center text-primary">Marcos Cordeiro</h3>
						<p class="text-center text-primary">Responsável pelo desenvolvimento do programa FÊNIX, professor de Spinning
							e Ginástica local na Unidade Barra.</p>
						<div style="margin: 0 auto; width:150px;">
							<a class="btn btn-warning" style="width:150px; background-color:#ff7800;">Dias e Horários</a>
						</div>
					</div>

				</div>

			</div>

		</div>

	</div>

	<div class="cover" style="height:400px;">
		<div class="background-image-fixed cover-image" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/07/homeBaixo.jpg);"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<h1 class="text-center text-inverse" contenteditable="true">PARE DE DIZER
						<br>“EU NÃO CONSIGO”</h1>
					<p class="text-inverse">VOCÊ PODE. CONHEÇA O FÊNIX.</p>
					<br>
					<br>
					<a class="btn btn-lg btn-warning">VEJA MAIS</a>
				</div>
			</div>
		</div>
	</div>
	<!-- END EDIT -->

	<!--
    <main>

        <section class="body-needs">
            <div class="column-1">
                <div class="top">
                    <div class="content">
                        <h2>Do que seu corpo precisa hoje?</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eget magna eu tortor venenatis auctor ut id purus. Quisque tincidunt id enim et laoreet. Nunc nulla ipsum, pellentesque nec metus in, finibus ultrices elit.</p>
                        <a href="#">Eu quero!</a>
                    </div>
                </div>
                <div class="bottom">
                    <div class="content">
                        <h2>Ginástica</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <a href="#">Veja mais</a>
                    </div>
                </div>
            </div><div class="column-2">
                <div class="content">
                    <h2>Ginástica</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    <a href="#">Veja mais</a>
                </div>
            </div>
        </section>
        <section class="activities">
            <div class="gridlock">
                <?php// $activities = homeGetActivities(); ?>

                <div class="swiper-container">

                    <div// class="swiper-wrapper">

                        <?php// foreach($activities as $atividade) { ?>
                            <div class="swiper-slide" style="background-image: url(<?php// echo $atividade['imagem'] ?>)" >
                                <h2><?php// echo $atividade['professor'] ?></h2>
                                <h3><?php// echo $atividade['nome'] ?></h3>
                                <small><?php// echo $atividade['descricao'] ?></small>
                                <a href="/horarios/q=<?php// echo $atividade['unidade'] ?>">Dias e Horários</a>
                            </div>
                        <?php// } ?>
                    </div>

                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div>
        </section>
        <section class="fenix">
            <h1>Pare de dizer<br /> "Eu não consigo!"</h1>
            <h2>Você pode. Conheça o Fênix.</h2>
            <a href="#">Comece agora</a>
        </section>
    </main>
                        -->


<?php get_footer(); ?>