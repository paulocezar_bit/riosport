<?php get_header(); ?>



	<div class="gridlock">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<main class="post_content">

				<?php the_title('<h1>','</h1>'); ?>

				<p class="post-info"><span><?php the_date(); ?></span></p>

				<?php the_content(); ?>

			</main>

		<?php endwhile; else : ?>

			<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>

		<?php endif; ?>

		<!--
        <aside>

            <?php/* $last_news = get_posts(array(

                'post_type'=>'post',

                'post_status'=>'publish',

                'posts_per_page'=> 3

            ));
            */
            ?>

            <h2>Últimas Notícias</h2>

            <?php// foreach($last_news as $single_new) { ?>

                <div class="single-new">

                    <img src="<?php// get_the_post_thumbnail_url($single_new->ID) ?>" alt="Thumb">

                    <h3><?php// echo $single_new->post_title ?></h3>

                </div>

            <?php// } ?>

        </aside>

    </div>
            -->

	</div>



<?php get_footer(); ?>