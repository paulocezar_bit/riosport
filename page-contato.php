<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="main">
        <p class="subtitle">Quer falar conosco? Mande uma mensagem que rapidinho respondemos. Caso prefira ligar, logo abaixo você encontra os telefones de cada unidade da Rio Sport</p>
        <div class="contact-form">
            <?php the_content(); ?>
        </div>
    </div>
<?php endwhile; else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

    <?php $unidades = contatoGetUnidades(); ?>

    <section class="contatos-das-unidades">
        <h1 class="unidades-title rsc_title">Contatos das Unidades</h1>
        <?php foreach($unidades as $unidade){ ?>
            <div class="unidade-item single-unit">
                <div class="contato-unidade-info">
                    <h3 class="title contato-unidade-info-elemento"><?php echo $unidade['unidade_name'] ?></h3>
                    <p class="email contato-unidade-info-elemento"><?php echo $unidade['unidade_email'] ?></p>
                    <p class="tel contato-unidade-info-elemento">Tel: <?php echo $unidade['unidade_telefone'] ?></p>
                </div>
                <div class="adress-box">
                <img src="<?php echo get_template_directory_uri() . '/assets/img/map-icon.png'; ?>" alt="Mapicon">
                    <p class="adress"><?php echo $unidade['unidade_endereco'] ?></p>
                </div>
            </div>
        <?php } ?>
    </section>



<?php get_footer(); ?>
