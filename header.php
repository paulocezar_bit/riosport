<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="UTF-8">

	<title><?php bloginfo( 'name' ); ?></title>



	<?php wp_head(); ?>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700|Poppins:300,400,700,800,900" rel="stylesheet">



</head>

<body <?php body_class(); ?>>

    <div class="header">

        <div id="navmenu">

            <div class="gridlock">

                <a href="/" class="logotipo"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logoriosport.png" alt="Rio Sport Center" /></a>

                <?php wp_nav_menu(array('theme_location' => 'header-menu','container_class'=>'menu-container')); ?>

                <ul class="social">

                    <li class="social-item"><a href="https://www.facebook.com/riosportacademia/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

                    <li class="social-item"><a href="https://www.instagram.com/riosportacademia/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>

                </ul>

            </div>

        </div>

        <div id="webdoor">
            <?php if(is_single()){
                getSingleHeader();
            } else{
                getWebdoor();
            } ?>

        </div>

    </div>









