<?php get_header(); ?>



<div class="gridlock">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<main class="post_content">

				<?php the_title('<h1>','</h1>'); ?>

				<p class="post-info"><span><?php the_date(); ?></span></p>

				<?php the_content(); ?>

			</main>

		<?php endwhile; else : ?>

			<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>

		<?php endif; ?>

</div>



<?php get_footer(); ?>

