<?php get_header();
?>
<div class="gridlock unidades">

<div class="unidade rsc_card odd">
    <div class="post-imagem rsc_card_imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/barra-01.jpg');"></div>
    <div class="post-info rsc_card_info">
        <h3 class="rsc_card_title">Barra da Tijuca</h3>
        <p class="rsc_card_text">Com o título de maior academia da América Latina, você pode imaginar toda a estrutura que vai encontrar nessa unidade. Mas, na verdade, a unidade da Barra da Tijuca é um complexo completo para as suas atividades...</p>
        <a href="http://www.riosportcenter.com.br/unidade/barra-da-tijuca" class="button-orange">Ver Unidade</a>
    </div>
</div>

<div class="unidade rsc_card even">
    <div class="post-imagem rsc_card_imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/recreio-01.jpg');"></div>
    <div class="post-info rsc_card_info">
        <h3 class="rsc_card_title">Recreio</h3>
        <p class="rsc_card_text">Bem-vindos a 1.300m2 da melhor estrutura para o seu bem estar e forma física. Na unidade Recreio você vai encontrar tudo o que precisa em cinco andares que atendem cerca de 115 aulas por semana...</p>
        <a href="http://www.riosportcenter.com.br/unidade/recreio" class="button-orange">Ver Unidade</a>
    </div>
</div>

<div class="unidade rsc_card odd">
    <div class="post-imagem rsc_card_imagem" style="background-image: url('http://www.riosportcenter.com.br/wp-content/uploads/2017/09/bonsucesso-01.jpg');"></div>
    <div class="post-info rsc_card_info">
        <h3 class="rsc_card_title">Bonsucesso</h3>
        <p class="rsc_card_text">O que há de mais novo para a sua saúde está na Rio Sport Center de Bonsucesso. Em 700m2, você pode aproveitar diversas atividades em aparelhos modernos e salas aconchegantes...</p>
        <a href="http://www.riosportcenter.com.br/unidade/bonsucesso" class="button-orange">Ver Unidade</a>
    </div>
</div>

</div>

<?php get_footer();
?>