<?php
/*
Template Name: Tapume
*/

wp_head();
?>
<html><head>
<title>Rio Sport Center</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <style>

        .fa{
            font-size: 26px;
            color: #FFFFFF;
            padding: 20px;
        }
    </style>
</head>
<body bgcolor="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

    <div style=" width: 100%; height: 100%; margin-top: 100px;">
        <div style="margin: 0 auto; width: 496px; height: 138px; color:#fff; "><img src="<?php echo get_template_directory_uri() . '/assets/img/logoriosport.png'?>" alt="Rio Sport" height="138px" width="496px"></div>
        <h2 style="font-family: verdana; color: white; text-align: center; font-weight:bold;">Um novo site em construção</h2>
        <p style="font-family: verdana; color: white; text-align: center; font-weight:bold;">Aguarde</p>
        <br>
        <p style="font-family: verdana; color: white; text-align: center; font-weight:bold;">faleconosco@riosportcenter.com.br</p>
    
        <br>
        <br>
        <p style="font-family: verdana; color: white; text-align: center; font-weight:regular;">Nas Redes Sociais<br>
    <div class="redes" style="padding-left: 47%;margin: auto 0; width:100%; color:#fff;"><a href="https://www.facebook.com/riosportacademia"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a href="https://www.instagram.com/riosportacademia"><i class="fa fa-instagram" aria-hidden="true"></i></a></div>

        <br>
        <br>
        <p style="font-family: verdana; color: white; text-align: center; font-weight:bold;">Barra da Tijuca</p>
        <p style="font-family: verdana; color: white; text-align: center; font-weight:bold;">Tel: (21) 3325-6644<br>
Av. Ayrton Senna, 2541<br>
Barra - Rio de Janeiro - RJ<br></p>
        
        <br>
        <br>
<p style="font-family: verdana; color: white; text-align: center; font-weight:bold;">Recreio</p>
        <p style="font-family: verdana; color: white; text-align: center; font-weight:bold;">Tel: (21) 2497-2020<br>
Av Alfredo Baltazar da Silveira, 1851<br>
Recreio dos Bandeirantes - Rio de Janeiro - RJ<br></p>
        
        <br>
        <br>
<p style="font-family: verdana; color: white; text-align: center; font-weight:bold;">Bonsucesso</p>
        <p style="font-family: verdana; color: white; text-align: center; font-weight:bold;">Tel: (21) (21) 3591-1905<br>
Rua Paris, 18<br>
Bonsucesso - Rio de Janeiro - RJ<br></p>
        
    </div>

<?php wp_footer(); ?>

</body></html>