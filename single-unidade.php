<?php get_header(); ?>



<div class="gridlock">

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<main>

			<h2 class="rsc_title">Estrutura</h2>

			<div class="unidade_content">
			
				<?php the_content(); ?>

			</div>

			<?php 
			$check_video = get_post_meta(get_the_ID(), 'unidade_video', true);
			if(!empty($check_video)): ?>
				<div class="unidade_video">
				<iframe width="100%" height="100%" src="<?php echo get_unidade_video(get_the_ID()); ?>" frameborder="0" allowfullscreen></iframe>
				</div>
			<?php endif; ?>

			<!--<h2 class="rsc_title">Fotos das Unidades</h2>

			<div class="unidade-gallery">

				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/barra-01.jpg);"></div>
				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/barra-02.jpg);"></div>
				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/barra-03.jpg);"></div>
				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/barra-04.jpg);"></div>
				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/barra-05.jpg);"></div>
				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/barra-06.jpg);"></div>
				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/barra-07.jpg);"></div>
				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/barra-08.jpg);"></div>

				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/recreio-01.jpg);"></div>
				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/recreio-02.jpg);"></div>
				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/recreio-03.jpg);"></div>
				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/recreio-04.jpg);"></div>
				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/recreio-05.jpg);"></div>
				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/recreio-06.jpg);"></div>
				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/recreio-07.jpg);"></div>
				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/recreio-08.jpg);"></div>

				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/bonsucesso-01.jpg);"></div>
				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/bonsucesso-02.jpg);"></div>
				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/bonsucesso-03.jpg);"></div>
				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/bonsucesso-04.jpg);"></div>
				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/bonsucesso-05.jpg);"></div>
				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/bonsucesso-06.jpg);"></div>
				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/bonsucesso-07.jpg);"></div>
				<div class="unidade-photo" style="background-image: url(http://www.riosportcenter.com.br/wp-content/uploads/2017/09/bonsucesso-08.jpg);"></div>

			</div>-->
			
			<p class="unidade_telefone"><?php echo get_post_meta(get_the_id(), 'unidade_telefone', true) ?></p>
			<p class="unidade_endereco"><?php echo get_post_meta(get_the_id(), 'unidade_endereco', true) ?></p>

		</main>

	<?php endwhile; else : ?>

		<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>

	<?php endif; ?>

</div>



<?php get_footer(); ?>